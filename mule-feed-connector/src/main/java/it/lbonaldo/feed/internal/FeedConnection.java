package it.lbonaldo.feed.internal;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeoutException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.mule.runtime.api.connection.ConnectionException;
import org.mule.runtime.http.api.HttpConstants.Method;
import org.mule.runtime.http.api.HttpService;
import org.mule.runtime.http.api.client.HttpClient;
import org.mule.runtime.http.api.client.HttpClientConfiguration;
import org.mule.runtime.http.api.domain.message.request.HttpRequest;
import org.mule.runtime.http.api.domain.message.request.HttpRequestBuilder;
import org.mule.runtime.http.api.domain.message.response.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class represents an extension connection just as example (there is no
 * real connection with anything here c:).
 */
public class FeedConnection {

	private final Logger LOGGER = LoggerFactory.getLogger(FeedConnection.class);
	private FeedConfiguration genConfig;
	private int connectionTimeout;
	private HttpClient httpClient;
	private HttpRequestBuilder httpRequestBuilder;

	public FeedConnection(HttpService httpService, FeedConfiguration gConfig, int cTimeout) {
		genConfig = gConfig;
		connectionTimeout = cTimeout;
		initHttpClient(httpService);
	}

	public void initHttpClient(HttpService httpService) {
		HttpClientConfiguration.Builder builder = new HttpClientConfiguration.Builder();
		builder.setName("lbonaldo-feed-connector");
		httpClient = httpService.getClientFactory().create(builder.build());
		httpRequestBuilder = HttpRequest.builder();
		httpClient.start();
	}

	public void invalidate() {
		httpClient.stop();
	}

	public boolean isConnected() throws Exception {

		String strUri = genConfig.getBlogUrl();

		HttpRequest request = httpRequestBuilder.method(Method.GET).uri(strUri).build();

		HttpResponse httpResponse = httpClient.send(request, connectionTimeout, false, null);

		if (httpResponse.getStatusCode() >= 200 && httpResponse.getStatusCode() < 300)
			return true;
		else
			throw new ConnectionException(
					"Error connecting to the server: Error Code " + httpResponse.getStatusCode() + "~" + httpResponse);
	}

	public String getRssUrl() {
		String url = null;

		try {
			LOGGER.info("Input Url: " + genConfig.getBlogUrl());
			Document doc = Jsoup.connect(genConfig.getBlogUrl()).get();
			LOGGER.info("Document: " + doc.title());
			Elements newsHeadlines = doc.select("head link[type=application/rss+xml]");
			LOGGER.info("Rss link found: " + newsHeadlines.size());
			// for (Element headline : newsHeadlines) {
			// LOGGER.info("link found: " + headline.toString());
			// }
			if (!newsHeadlines.isEmpty()) {
				url = newsHeadlines.get(0).attr("href");
				LOGGER.info("rss url result: " + url);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return url;
	}

	public InputStream getRss() {

		HttpResponse httpResponse = null;
		String strUri = getRssUrl();

		try {

			HttpRequest request = httpRequestBuilder.method(Method.GET).uri(strUri).build();
			httpResponse = httpClient.send(request, connectionTimeout, false, null);
			System.out.println(httpResponse);
			return httpResponse.getEntity().getContent();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
