package it.lbonaldo.feed.internal;

import static org.mule.runtime.extension.api.annotation.param.MediaType.ANY;

import java.io.InputStream;

import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;

/**
 * This class is a container for operations, every public method in this class
 * will be taken as an extension operation.
 */
public class FeedOperations {

	@MediaType(value = ANY, strict = false)
	@DisplayName("GET RSS")
	public InputStream getRss(@Connection FeedConnection connection) {
		return connection.getRss();
	}

	@MediaType(value = ANY, strict = false)
	@DisplayName("GET RSS URL")
	public String getRssUrl(@Connection FeedConnection connection) {
		return connection.getRssUrl();
	}
}
