package it.lbonaldo.feed.internal;

import org.mule.runtime.extension.api.annotation.Operations;
import org.mule.runtime.extension.api.annotation.connectivity.ConnectionProviders;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;

/**
 * This class represents an extension configuration, values set in this class are commonly used across multiple
 * operations since they represent something core from the extension.
 */
@Operations(FeedOperations.class)
@ConnectionProviders(FeedConnectionProvider.class)
public class FeedConfiguration {

	private static final String GENL = "General";

	@Parameter
	@Placement(tab = GENL)
	@DisplayName("Blog's url")
	// @Expression(org.mule.runtime.api.meta.ExpressionSupport.NOT_SUPPORTED)
	private String blogUrl;

	public String getBlogUrl() {
		return blogUrl;
	}

	public void setBlogUrl(String blogUrl) {
		this.blogUrl = blogUrl;
	}	
	
}
