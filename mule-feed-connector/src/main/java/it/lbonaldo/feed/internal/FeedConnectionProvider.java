package it.lbonaldo.feed.internal;

import javax.inject.Inject;

import org.mule.runtime.api.connection.CachedConnectionProvider;
import org.mule.runtime.api.connection.ConnectionException;
import org.mule.runtime.api.connection.ConnectionProvider;
import org.mule.runtime.api.connection.ConnectionValidationResult;
import org.mule.runtime.api.connection.PoolingConnectionProvider;
import org.mule.runtime.api.meta.ExternalLibraryType;
import org.mule.runtime.extension.api.annotation.ExternalLib;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.http.api.HttpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This class (as it's name implies) provides connection instances and the funcionality to disconnect and validate those
 * connections.
 * <p>
 * All connection related parameters (values required in order to create a connection) must be
 * declared in the connection providers.
 * <p>
 * This particular example is a {@link PoolingConnectionProvider} which declares that connections resolved by this provider
 * will be pooled and reused. There are other implementations like {@link CachedConnectionProvider} which lazily creates and
 * caches connections or simply {@link ConnectionProvider} if you want a new connection each time something requires one.
 */
@ExternalLib(
		name = "JSOUP", 
		description = "Libraty for html parse support", 
		nameRegexpMatcher = "(.*)\\.jar", 
		requiredClassName = "org.jsoup.Jsoup", 
		coordinates = "org.jsoup:jsoup:1.13.1", 
		type = ExternalLibraryType.DEPENDENCY,
		optional = false
)
public class FeedConnectionProvider implements PoolingConnectionProvider<FeedConnection> {

	 private final Logger LOGGER = LoggerFactory.getLogger(FeedConnectionProvider.class);
	 
	 @Parameter
	 @Placement(tab = "Advanced")
	 @Optional(defaultValue = "5000")
	 int connectionTimeout;

	 @ParameterGroup(name = "Connection")
	 FeedConfiguration genConfig;

	 @Inject
	 private HttpService httpService; 
	 
	 /**
	  * 
	  */
	 @Override
	 public FeedConnection connect() throws ConnectionException {
	      return new FeedConnection(httpService, genConfig, connectionTimeout);
	 }
	 /**
	  * 
	  */
	 @Override
	 public void disconnect(FeedConnection connection) {
	      try {
	            connection.invalidate();
	      } catch (Exception e) {
	            LOGGER.error("Error while disconnecting to Weather Channel " + e.getMessage(), e);
	      }
	 }
	 /**
	  * 
	  */
	 @Override
	 public ConnectionValidationResult validate(FeedConnection connection) {
	      ConnectionValidationResult result;
	      try {
	           if(connection.isConnected()){
	                  result = ConnectionValidationResult.success();
	            } else {
	                  result = ConnectionValidationResult.failure("Connection Failed", new Exception());
	            }
	     } catch (Exception e) {
	           result = ConnectionValidationResult.failure("Connection failed: " + e.getMessage(), new Exception());
	     }
	   return result;
	 }
}
