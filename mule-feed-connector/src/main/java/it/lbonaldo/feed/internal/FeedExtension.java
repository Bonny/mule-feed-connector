package it.lbonaldo.feed.internal;

import org.mule.runtime.api.meta.Category;
import org.mule.runtime.extension.api.annotation.Extension;
import org.mule.runtime.extension.api.annotation.Operations;
import org.mule.runtime.extension.api.annotation.connectivity.ConnectionProviders;
import org.mule.runtime.extension.api.annotation.dsl.xml.Xml;


/**
 * This is the main class of an extension, is the entry point from which configurations, connection providers, operations
 * and sources are going to be declared.
 */
@Xml(prefix = "feed")
@Extension(
		name = "Feed", 
		vendor = "lbonaldo.it", 
		category = Category.COMMUNITY
)
@ConnectionProviders(FeedConnectionProvider.class)
@Operations({FeedOperations.class})
public class FeedExtension {

}
